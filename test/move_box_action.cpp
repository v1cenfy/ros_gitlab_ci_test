#include <ros/ros.h>
#include <gtest/gtest.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <actionlib/client/simple_action_client.h>
#pragma GCC diagnostic pop
#include <actionlib/client/terminal_state.h>
#include <box_robot_definition/MoveBoxAction.h>

TEST(TESTSuite, sendGoal)
{
  // Create the action client
  actionlib::SimpleActionClient<box_robot_definition::MoveBoxAction> ac("move_box", true);

  bool server_exists(ac.waitForServer(ros::Duration(2)));
  EXPECT_TRUE(server_exists);

  // Send a goal to the action
  box_robot_definition::MoveBoxGoal goal;
  goal.z_angle = M_PI;;

  ac.sendGoal(goal);
  bool finished_before_timeout = ac.waitForResult(ros::Duration(2));
  actionlib::SimpleClientGoalState state = ac.getState();

  EXPECT_TRUE(finished_before_timeout);
  EXPECT_EQ(state, actionlib::SimpleClientGoalState::SUCCEEDED);
}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "test_move_box_action");
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

