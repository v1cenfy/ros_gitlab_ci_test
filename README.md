 [![ROS](http://www.ros.org/wp-content/uploads/2013/10/rosorg-logo1.png)](https://gitlab.com/VictorLamoine/ros_gitlab_ci) GitLab CI test
===

[![build status](https://gitlab.com/VictorLamoine/ros_gitlab_ci_test/badges/kinetic/build.svg)](https://gitlab.com/VictorLamoine/ros_gitlab_ci_test/commits/kinetic)

This is a test example package for [ROS GitLab CI](https://gitlab.com/VictorLamoine/ros_gitlab_ci); it includes:
- `catkin_make` build
- `catkin tools` build
- `catkin_make` tests
- `catkin tools` tests

It does not test:
- `wstool` file

